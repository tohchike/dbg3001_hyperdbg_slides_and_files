1
00:00:00,000 --> 00:00:05,040
Hello everyone welcome to the 10th part

2
00:00:03,000 --> 00:00:07,620
of the tutorial reversing with HyperDbg

3
00:00:05,040 --> 00:00:10,679
in this part we're gonna talk

4
00:00:07,620 --> 00:00:14,280
about IO debugging here is a brief

5
00:00:10,679 --> 00:00:18,300
outline of this session we're gonna see

6
00:00:14,280 --> 00:00:21,800
about external interrupts you will see

7
00:00:18,300 --> 00:00:25,380
communicating with different peripherals

8
00:00:21,800 --> 00:00:28,920
port-mapped IO or PMIO devices memory map.io

9
00:00:25,380 --> 00:00:32,160
and then we compare PMIO and MMIO then

10
00:00:28,920 --> 00:00:35,399
we have a hands-on and in which we

11
00:00:32,160 --> 00:00:38,160
monitor and modify IO ports for PS2

12
00:00:35,399 --> 00:00:40,920
keyboard and now let's talk a little bit

13
00:00:38,160 --> 00:00:43,320
about external interrupts this is the

14
00:00:40,920 --> 00:00:45,680
same type of that we also covered in the

15
00:00:43,320 --> 00:00:49,379
previous session

16
00:00:45,680 --> 00:00:52,920
in previous sessions that how interrupts

17
00:00:49,379 --> 00:00:55,440
are handled in processor and how a

18
00:00:52,920 --> 00:00:58,379
processor and how HyperDbg can

19
00:00:55,440 --> 00:01:01,980
intercept interrupts and then and then

20
00:00:58,379 --> 00:01:05,339
perform modification and we saw that

21
00:01:01,980 --> 00:01:08,100
it's possible to do these tests by using

22
00:01:05,339 --> 00:01:09,960
bang interrupt command for example in

23
00:01:08,100 --> 00:01:12,920
most of the cases when external device

24
00:01:09,960 --> 00:01:16,080
is any external devices that wants to

25
00:01:12,920 --> 00:01:18,840
notify the processor that something is

26
00:01:16,080 --> 00:01:22,380
happening in that special device then it

27
00:01:18,840 --> 00:01:24,659
creates a interrupt and this way it

28
00:01:22,380 --> 00:01:27,360
notifies the processor for example when

29
00:01:24,659 --> 00:01:29,939
we press a key on or keyboard then

30
00:01:27,360 --> 00:01:32,159
trapped is invoked and it's the

31
00:01:29,939 --> 00:01:34,560
responsibility of the processor to

32
00:01:32,159 --> 00:01:36,180
handle the keyboard and and it just

33
00:01:34,560 --> 00:01:38,460
notifies the operating system and

34
00:01:36,180 --> 00:01:40,979
everything is handled inside the

35
00:01:38,460 --> 00:01:43,619
operating system so most of the times

36
00:01:40,979 --> 00:01:46,740
when they Interrupters receive it's the

37
00:01:43,619 --> 00:01:50,939
responsibility of APIC device to handle

38
00:01:46,740 --> 00:01:53,759
and deliver it to the processor and and

39
00:01:50,939 --> 00:01:57,240
generally we have two types of APIC

40
00:01:53,759 --> 00:02:00,060
devices x2APIC and xAPIC devices which

41
00:01:57,240 --> 00:02:03,420
is an older version first one handle

42
00:02:00,060 --> 00:02:06,960
through MSR registers and the second one

43
00:02:03,420 --> 00:02:10,259
or xAPIC is handled by base addressed by a

44
00:02:06,960 --> 00:02:12,840
physical address as HyperDbg is a

45
00:02:10,259 --> 00:02:16,080
hypervisor level debugger then it's

46
00:02:12,840 --> 00:02:19,680
notified about the interrupts before the

47
00:02:16,080 --> 00:02:21,720
operating system so it's in this stage

48
00:02:19,680 --> 00:02:23,700
We could decide whether we want to pass

49
00:02:21,720 --> 00:02:26,120
the interrupt to the operating system or

50
00:02:23,700 --> 00:02:28,860
we can handle it or ignore it without

51
00:02:26,120 --> 00:02:31,260
re-injecting the event or the interrupt

52
00:02:28,860 --> 00:02:34,020
to the operating system now let's see

53
00:02:31,260 --> 00:02:37,020
some of the basic concepts relating to

54
00:02:34,020 --> 00:02:39,720
the communication of the peripherals in

55
00:02:37,020 --> 00:02:42,660
HyperDbg for intercepting the

56
00:02:39,720 --> 00:02:45,120
communication there are two options the

57
00:02:42,660 --> 00:02:48,360
first option is to manage external

58
00:02:45,120 --> 00:02:50,459
devices by intercepting interrupts for

59
00:02:48,360 --> 00:02:53,340
example we got notified whenever a

60
00:02:50,459 --> 00:02:56,340
device wants to connect to the operating

61
00:02:53,340 --> 00:02:58,440
system to send an input or an output

62
00:02:56,340 --> 00:03:00,840
this is done by using the interrupt

63
00:02:58,440 --> 00:03:04,379
command and there are also other

64
00:03:00,840 --> 00:03:07,440
possible ways of handling or debugging 

65
00:03:04,379 --> 00:03:10,319
IO devices basically if I once generally

66
00:03:07,440 --> 00:03:12,900
speak about it that we have two types of

67
00:03:10,319 --> 00:03:15,780
communication between the devices and

68
00:03:12,900 --> 00:03:20,459
the processor is either port mapped IO

69
00:03:15,780 --> 00:03:23,599
or PMIO or memory mapped IO or MMIO I

70
00:03:20,459 --> 00:03:25,980
did these methods are responsible for

71
00:03:23,599 --> 00:03:28,260
performing input and output and

72
00:03:25,980 --> 00:03:31,319
communicating between different prefers

73
00:03:28,260 --> 00:03:34,200
processors and the operating system so

74
00:03:31,319 --> 00:03:37,920
if the device is in most of the devices

75
00:03:34,200 --> 00:03:41,400
that are connected through the PCIe or

76
00:03:37,920 --> 00:03:44,580
PCI these devices generally use memory

77
00:03:41,400 --> 00:03:47,340
mapped IO and the initial configuration

78
00:03:44,580 --> 00:03:52,739
of these devices are done by using PMIO

79
00:03:47,340 --> 00:03:55,319
MMIO is the preferred method in x86

80
00:03:52,739 --> 00:03:59,519
based processors and the thing is that

81
00:03:55,319 --> 00:04:01,940
AMD did not for the 64-bit processor AMD

82
00:03:59,519 --> 00:04:05,400
did not extent that IO port

83
00:04:01,940 --> 00:04:09,299
instructions when defining the new x86

84
00:04:05,400 --> 00:04:12,060
architecture now let's see a port mapped

85
00:04:09,299 --> 00:04:14,939
IO or PMIO in detail where's my fio

86
00:04:12,060 --> 00:04:17,699
which is also called isolated ious two

87
00:04:14,939 --> 00:04:20,459
instructions for performing IO

88
00:04:17,699 --> 00:04:23,880
operations these instructions are in and

89
00:04:20,459 --> 00:04:27,660
out and these instructions have several

90
00:04:23,880 --> 00:04:29,580
variants so some of them copy one byte two

91
00:04:27,660 --> 00:04:31,940
bytes or four bytes and it's done

92
00:04:29,580 --> 00:04:34,560
between eax and a special port

93
00:04:31,940 --> 00:04:38,160
HyperDbg the other hand exports

94
00:04:34,560 --> 00:04:41,580
this functionality or make the debugging

95
00:04:38,160 --> 00:04:45,720
of the IO ports possible by using two

96
00:04:41,580 --> 00:04:48,660
commands ioin or bang ioin and bang or

97
00:04:45,720 --> 00:04:51,840
exclamation mark ioout the first one is

98
00:04:48,660 --> 00:04:53,759
used for intercepting in instructions

99
00:04:51,840 --> 00:04:56,759
and the second one is used for

100
00:04:53,759 --> 00:04:59,520
intercepting out instructions and we

101
00:04:56,759 --> 00:05:02,699
could use the script engine here to

102
00:04:59,520 --> 00:05:05,340
notify about the about this

103
00:05:02,699 --> 00:05:07,500
communication between peripherals even

104
00:05:05,340 --> 00:05:10,620
before the operating system and perform

105
00:05:07,500 --> 00:05:14,340
some changes and the operating system

106
00:05:10,620 --> 00:05:17,940
will never know that the input or output

107
00:05:14,340 --> 00:05:21,360
is modified and here is a simple script

108
00:05:17,940 --> 00:05:23,880
that shows a rare for example this port

109
00:05:21,360 --> 00:05:26,940
IO port is read by using a simple

110
00:05:23,880 --> 00:05:30,780
printf in the script engine other than

111
00:05:26,940 --> 00:05:35,100
that PMIO we have we also have MMIO here

112
00:05:30,780 --> 00:05:39,240
or memory mapped IO devices memory is

113
00:05:35,100 --> 00:05:41,699
not just RAM addresses and the memory

114
00:05:39,240 --> 00:05:43,979
itself is occupied by RAM and a small

115
00:05:41,699 --> 00:05:46,440
fraction of total address spaces are

116
00:05:43,979 --> 00:05:50,400
relating to the processor the reminder

117
00:05:46,440 --> 00:05:53,520
of the memory itself is used for MMIO

118
00:05:50,400 --> 00:05:56,699
devices including the PCI and PCIe

119
00:05:53,520 --> 00:05:59,900
devices and the processor routes these

120
00:05:56,699 --> 00:06:02,460
accesses to these addresses by company

121
00:05:59,900 --> 00:06:04,620
appropriate device registers whenever

122
00:06:02,460 --> 00:06:07,380
the processor wants to enamorate the

123
00:06:04,620 --> 00:06:10,080
BIOS or the operating system wants to

124
00:06:07,380 --> 00:06:13,020
configure this space at the

125
00:06:10,080 --> 00:06:16,320
initialization at this point there is a

126
00:06:13,020 --> 00:06:18,419
special way to access the devices and

127
00:06:16,320 --> 00:06:23,520
it's called PCI configuration address

128
00:06:18,419 --> 00:06:25,860
space in this way to IO two PMIO ports

129
00:06:23,520 --> 00:06:28,680
which you can see its address here are

130
00:06:25,860 --> 00:06:31,620
used to access the configuration space

131
00:06:28,680 --> 00:06:34,319
and later it's the responsibility of the

132
00:06:31,620 --> 00:06:37,919
either BIOS or the operating system to

133
00:06:34,319 --> 00:06:40,680
set up the MMIO ranges so in a typical

134
00:06:37,919 --> 00:06:44,819
operating system firmware or BIOS

135
00:06:40,680 --> 00:06:48,240
queries all the PCI buses at the start

136
00:06:44,819 --> 00:06:51,419
time at the startup time by using PCI

137
00:06:48,240 --> 00:06:54,479
configuration space and find the devices

138
00:06:51,419 --> 00:06:57,360
present and also configure the system

139
00:06:54,479 --> 00:06:59,960
resources like memory space IO space

140
00:06:57,360 --> 00:07:03,000
interrupt lines and etc. then it

141
00:06:59,960 --> 00:07:06,419
allocates the resource and tells the

142
00:07:03,000 --> 00:07:08,780
device about its allocation so in

143
00:07:06,419 --> 00:07:11,340
general PCI configurations but also

144
00:07:08,780 --> 00:07:13,740
contains a small amount of device type

145
00:07:11,340 --> 00:07:17,160
information helps the operating system

146
00:07:13,740 --> 00:07:21,060
to choose the appropriate device

147
00:07:17,160 --> 00:07:24,300
driver for it and also as MMIO is 

148
00:07:21,060 --> 00:07:26,880
preferred over PMIO or as we mentioned it

149
00:07:24,300 --> 00:07:29,539
before and the reason for that is

150
00:07:26,880 --> 00:07:33,120
because the instructions that perform

151
00:07:29,539 --> 00:07:34,919
portmapped IO are limited to just one

152
00:07:33,120 --> 00:07:39,240
instructions you can all you can only

153
00:07:34,919 --> 00:07:42,539
have eax ax or al or just one register

154
00:07:39,240 --> 00:07:45,360
for this PMIO instructions and the other

155
00:07:42,539 --> 00:07:48,900
registers that the data can be moved in

156
00:07:45,360 --> 00:07:52,259
and out is a byte size immediate value

157
00:07:48,900 --> 00:07:55,740
in the instruction or a value that is

158
00:07:52,259 --> 00:07:58,979
available in edx register which

159
00:07:55,740 --> 00:08:01,440
determines which port is the source or

160
00:07:58,979 --> 00:08:04,620
the destination force for this PMIO

161
00:08:01,440 --> 00:08:07,979
instruction to transfer the data and all

162
00:08:04,620 --> 00:08:11,460
of the general purpose registers can

163
00:08:07,979 --> 00:08:14,520
be used to receive or send the data in

164
00:08:11,460 --> 00:08:17,220
for on the other hand because in

165
00:08:14,520 --> 00:08:20,060
memory mapped IO all of the general

166
00:08:17,220 --> 00:08:23,340
purpose registers can be used for

167
00:08:20,060 --> 00:08:27,120
sending a receiving data from and into

168
00:08:23,340 --> 00:08:30,539
memory so a memory mapped IO uses fewer

169
00:08:27,120 --> 00:08:34,440
instructions and and ultimately can run

170
00:08:30,539 --> 00:08:38,039
faster than IO or PMIO if you want to

171
00:08:34,440 --> 00:08:41,039
just perform some IO debugging with MMIO

172
00:08:38,039 --> 00:08:43,680
devices you can use the old !monitor

173
00:08:41,039 --> 00:08:46,680
command which is responsible for

174
00:08:43,680 --> 00:08:49,019
handling IO debugging for MMIO devices

175
00:08:46,680 --> 00:08:52,140
and also if you want to just see the

176
00:08:49,019 --> 00:08:55,019
base address for different devices or

177
00:08:52,140 --> 00:08:57,779
the base physical address for different

178
00:08:55,019 --> 00:08:59,519
devices or where the device address is

179
00:08:57,779 --> 00:09:01,980
mapped into the memory and there are

180
00:08:59,519 --> 00:09:05,160
also some resources I will try to

181
00:09:01,980 --> 00:09:08,040
introduce some of these devices later in

182
00:09:05,160 --> 00:09:10,019
this slides but generally we can see

183
00:09:08,040 --> 00:09:13,200
that for example this device is

184
00:09:10,019 --> 00:09:15,660
allocated and the map to the address

185
00:09:13,200 --> 00:09:19,800
shown in the picture now let's compare

186
00:09:15,660 --> 00:09:22,620
PMIO and MMIO devices or PMIO or

187
00:09:19,800 --> 00:09:25,620
isolated IO different addresses spaces

188
00:09:22,620 --> 00:09:28,260
are used for computer memory at IO

189
00:09:25,620 --> 00:09:32,100
device devices have a dedicated address

190
00:09:28,260 --> 00:09:35,279
space while in a memory mapped IO or

191
00:09:32,100 --> 00:09:38,940
MMIO the same address is used for both

192
00:09:35,279 --> 00:09:41,760
memory and IO devices in case of PMIO

193
00:09:38,940 --> 00:09:45,120
separate con control units and control

194
00:09:41,760 --> 00:09:49,860
instructions are used in case of IO

195
00:09:45,120 --> 00:09:52,320
devices and in MMIO control units and

196
00:09:49,860 --> 00:09:55,200
instructions are same for memory and IO

197
00:09:52,320 --> 00:09:58,680
devices the PMIO has a more complex and

198
00:09:55,200 --> 00:10:03,360
costlier than memory mapped IO as more

199
00:09:58,680 --> 00:10:06,080
passes are needed by in MMIO it's easier

200
00:10:03,360 --> 00:10:09,240
to build and cheaper and cheap because

201
00:10:06,080 --> 00:10:12,600
it's less complex the entire address

202
00:10:09,240 --> 00:10:15,480
space can be used by memory as IO

203
00:10:12,600 --> 00:10:18,899
devices have separate other spaces in

204
00:10:15,480 --> 00:10:21,660
PMIO while in MMIO some parts of

205
00:10:18,899 --> 00:10:24,480
other space of computer memory is

206
00:10:21,660 --> 00:10:27,180
consumed by IO devices as address space

207
00:10:24,480 --> 00:10:29,480
to share for the PMIO computer memory

208
00:10:27,180 --> 00:10:31,860
and IO devices use different control

209
00:10:29,480 --> 00:10:34,980
instructions for read and write for

210
00:10:31,860 --> 00:10:38,040
example in or out instructions are used

211
00:10:34,980 --> 00:10:40,980
while in MMIO computer memory and IO

212
00:10:38,040 --> 00:10:43,620
devices can both use the same set of

213
00:10:40,980 --> 00:10:45,240
read and write instructions all of them

214
00:10:43,620 --> 00:10:48,120
use move instructions for example

215
00:10:45,240 --> 00:10:51,540
separate control bus is used for the

216
00:10:48,120 --> 00:10:54,959
computer memory and IO devices for PMIO

217
00:10:51,540 --> 00:10:58,800
and the same address and data bus are

218
00:10:54,959 --> 00:11:02,100
used and while in MMIO address data and

219
00:10:58,800 --> 00:11:06,240
control bus are same for memory and IO

220
00:11:02,100 --> 00:11:10,260
devices now let's see an example of how

221
00:11:06,240 --> 00:11:14,600
we can analyze and modify the codes

222
00:11:10,260 --> 00:11:18,959
relating to a PC PS2 keyboard actually

223
00:11:14,600 --> 00:11:22,019
PS2 keyboards are some old versions of

224
00:11:18,959 --> 00:11:24,839
keyboards that are currently not used

225
00:11:22,019 --> 00:11:27,660
but I think it's it would be really good

226
00:11:24,839 --> 00:11:30,480
for an example because it's simple the

227
00:11:27,660 --> 00:11:33,720
protocol is really simple and easy to

228
00:11:30,480 --> 00:11:36,000
understand and we can go after that we

229
00:11:33,720 --> 00:11:39,540
could extend our knowledge to perform

230
00:11:36,000 --> 00:11:43,200
the debugging of more complex IO devices

231
00:11:39,540 --> 00:11:48,079
so let's see I just noticed that in most

232
00:11:43,200 --> 00:11:52,560
of the VMware versions they use

233
00:11:48,079 --> 00:11:54,720
they use a PS2 keyboard

234
00:11:52,560 --> 00:11:56,880
they still use but for the emulation

235
00:11:54,720 --> 00:11:59,180
they use PS3 keyboard

236
00:11:56,880 --> 00:12:03,200
so let's again

237
00:11:59,180 --> 00:12:03,200
turn on

238
00:12:03,240 --> 00:12:09,560
the WinDbg in order to disable the

239
00:12:06,060 --> 00:12:09,560
driver signature enforcement

240
00:12:09,660 --> 00:12:16,339
and

241
00:12:11,700 --> 00:12:16,339
see the VMware workstation

242
00:12:22,040 --> 00:12:29,300
in VMware Workstation we could also

243
00:12:25,920 --> 00:12:29,300
use system information

244
00:12:34,640 --> 00:12:41,220
system information

245
00:12:37,860 --> 00:12:44,639
in hardware resources there are IRQ

246
00:12:41,220 --> 00:12:49,620
lines and IO devices as you can see

247
00:12:44,639 --> 00:12:52,220
here IRQ1 is used for standard PS3

248
00:12:49,620 --> 00:12:52,220
keyboards

249
00:12:52,560 --> 00:12:59,279
and if we go to the

250
00:12:55,279 --> 00:12:59,279
IO resources

251
00:12:59,300 --> 00:13:05,820
we could see different IO resources that

252
00:13:03,120 --> 00:13:08,880
are used for the system and one of them

253
00:13:05,820 --> 00:13:13,920
is of course PS2 keyboard

254
00:13:08,880 --> 00:13:19,160
which are which is available on this

255
00:13:13,920 --> 00:13:24,240
special port 60 port in hex format and

256
00:13:19,160 --> 00:13:27,980
64 in also in hex formats so we could

257
00:13:24,240 --> 00:13:33,660
use it as a starting point to see how

258
00:13:27,980 --> 00:13:36,060
these PS2 keyboards work so I just try

259
00:13:33,660 --> 00:13:38,180
to connect to HyperDbg I run it from

260
00:13:36,060 --> 00:13:43,500
the source code

261
00:13:38,180 --> 00:13:43,500
perform debugging

262
00:13:45,360 --> 00:13:49,100
connecting to the serial port

263
00:13:51,360 --> 00:13:55,519
this time I use debug version

264
00:14:07,620 --> 00:14:14,839
um well no after performing this

265
00:14:12,000 --> 00:14:19,800
synchronization I just try to

266
00:14:14,839 --> 00:14:23,639
monitor monitor IO monitor this

267
00:14:19,800 --> 00:14:25,399
address 16 hex format to see what

268
00:14:23,639 --> 00:14:30,000
happens

269
00:14:25,399 --> 00:14:33,000
so I use if you see the help of IO

270
00:14:30,000 --> 00:14:36,959
in command

271
00:14:33,000 --> 00:14:38,760
it gets the port address and process ID

272
00:14:36,959 --> 00:14:40,620
and different items related to the

273
00:14:38,760 --> 00:14:46,800
events but we're only interested in port

274
00:14:40,620 --> 00:14:49,139
address and so I try to run IO in in 60

275
00:14:46,800 --> 00:14:52,620
port address in hex format everything in

276
00:14:49,139 --> 00:14:56,639
hybrid which is an X format

277
00:14:52,620 --> 00:15:02,399
I'll continue the debuggee and now

278
00:14:56,639 --> 00:15:05,300
perform some press some keys here

279
00:15:02,399 --> 00:15:11,540
and everything just halts so let's just

280
00:15:05,300 --> 00:15:11,540
come back to the VM I will put it here

281
00:15:16,440 --> 00:15:22,440
yeah

282
00:15:17,639 --> 00:15:27,300
so as we can see this address is

283
00:15:22,440 --> 00:15:30,839
able to read data from the keyboard

284
00:15:27,300 --> 00:15:35,760
it's in um bus driver and this is the

285
00:15:30,839 --> 00:15:39,839
function so let's see this function

286
00:15:35,760 --> 00:15:42,720
by using the U command

287
00:15:39,839 --> 00:15:45,839
we want to disassemble this address and

288
00:15:42,720 --> 00:15:48,620
as we can see here it seems to be a

289
00:15:45,839 --> 00:15:52,740
simple function that only performs

290
00:15:48,620 --> 00:15:56,000
in a simple return after in

291
00:15:52,740 --> 00:15:57,779
instruction is executed so

292
00:15:56,000 --> 00:15:59,600
uh

293
00:15:57,779 --> 00:16:02,519
this is the address

294
00:15:59,600 --> 00:16:05,760
this is the address that we we are

295
00:16:02,519 --> 00:16:08,399
interested in intercepting we have

296
00:16:05,760 --> 00:16:12,360
several options here we could

297
00:16:08,399 --> 00:16:15,800
intercept interrupts or for example

298
00:16:12,360 --> 00:16:18,779
or also we can also use in instruction

299
00:16:15,800 --> 00:16:20,279
to just modify the results of any

300
00:16:18,779 --> 00:16:24,180
instruction

301
00:16:20,279 --> 00:16:27,420
there's also another option which might

302
00:16:24,180 --> 00:16:31,860
be also which might be a better option

303
00:16:27,420 --> 00:16:33,720
to just put a simple EPT hook on this

304
00:16:31,860 --> 00:16:36,720
address because after running this

305
00:16:33,720 --> 00:16:38,519
instruction then when it when the

306
00:16:36,720 --> 00:16:42,420
processor wants to execute the next

307
00:16:38,519 --> 00:16:45,320
instructions already has the value of

308
00:16:42,420 --> 00:16:50,759
the input to the keyboard

309
00:16:45,320 --> 00:16:55,380
both of these ways can be used

310
00:16:50,759 --> 00:17:00,860
now let's use the second option here

311
00:16:55,380 --> 00:17:00,860
I already provide some

312
00:17:00,920 --> 00:17:06,959
resources here and a route test scripts

313
00:17:03,899 --> 00:17:09,600
you can see the script on the tutorial

314
00:17:06,959 --> 00:17:11,059
part

315
00:17:09,600 --> 00:17:14,040
no no no

316
00:17:11,059 --> 00:17:16,319
this this is a really good resource

317
00:17:14,040 --> 00:17:18,199
about it explain different interrupts

318
00:17:16,319 --> 00:17:21,540
IRQ lines

319
00:17:18,199 --> 00:17:24,360
I think it's better to read it if you

320
00:17:21,540 --> 00:17:28,740
you want to get familiar with PS2

321
00:17:24,360 --> 00:17:32,160
keyboards and the how general interrupt

322
00:17:28,740 --> 00:17:35,640
lines work on x86 plus so another

323
00:17:32,160 --> 00:17:40,440
resource which explains directly about

324
00:17:35,640 --> 00:17:41,940
PS3 keyboards is this it also

325
00:17:40,440 --> 00:17:45,320
provides

326
00:17:41,940 --> 00:17:49,679
information about different

327
00:17:45,320 --> 00:17:51,919
code sets or scan codes sets of these

328
00:17:49,679 --> 00:17:56,880
PS2 keyboards

329
00:17:51,919 --> 00:18:00,480
so I just try to put a simple EPT hook

330
00:17:56,880 --> 00:18:04,640
on this address because after this

331
00:18:00,480 --> 00:18:07,679
address al contains the scan code

332
00:18:04,640 --> 00:18:11,460
there are two scan codes for PS2

333
00:18:07,679 --> 00:18:13,039
keyboards both of them are mentioned

334
00:18:11,460 --> 00:18:17,580
here

335
00:18:13,039 --> 00:18:19,980
but VMware Workstation uses the first or

336
00:18:17,580 --> 00:18:23,120
emulates the keyboard in the first scan

337
00:18:19,980 --> 00:18:23,120
code set so

338
00:18:23,840 --> 00:18:31,760
we simply write a simple s script to

339
00:18:28,679 --> 00:18:31,760
just print the

340
00:18:31,980 --> 00:18:34,760
uh

341
00:18:36,240 --> 00:18:40,340
code or print the

342
00:18:42,179 --> 00:18:48,480
different keys that are pressed we could

343
00:18:44,820 --> 00:18:52,160
also it's it's somehow like a simple

344
00:18:48,480 --> 00:18:56,460
keylogger hypervisor based keylogger

345
00:18:52,160 --> 00:19:00,980
that is notified about the about

346
00:18:56,460 --> 00:19:00,980
pressing off a key before

347
00:19:01,260 --> 00:19:08,660
even the operating system is notified so

348
00:19:05,400 --> 00:19:08,660
I just use

349
00:19:12,539 --> 00:19:18,380
this simple printf here and print Al

350
00:19:15,720 --> 00:19:18,380
instruction

351
00:19:27,179 --> 00:19:33,539
before that I try to clear all

352
00:19:30,539 --> 00:19:33,539
events

353
00:19:42,419 --> 00:19:50,880
and now I run the target debuggee and try

354
00:19:49,260 --> 00:19:54,679
to

355
00:19:50,880 --> 00:19:54,679
press some keys here

356
00:19:56,100 --> 00:20:04,860
like hello as we can see

357
00:19:59,840 --> 00:20:11,000
by entering any keys we could see how

358
00:20:04,860 --> 00:20:11,000
it's pressed and see the key code here

359
00:20:11,840 --> 00:20:19,280
now I

360
00:20:16,020 --> 00:20:22,260
just try to this this can be a simple

361
00:20:19,280 --> 00:20:24,780
hypervisor based keylogger I try to just

362
00:20:22,260 --> 00:20:28,940
uh

363
00:20:24,780 --> 00:20:28,940
clear all

364
00:20:29,539 --> 00:20:39,960
events and now let's just try to

365
00:20:33,419 --> 00:20:39,960
manipulate it by for example

366
00:20:41,160 --> 00:20:45,120
when

367
00:20:42,900 --> 00:20:47,039
uh

368
00:20:45,120 --> 00:20:52,200
let's see

369
00:20:47,039 --> 00:20:57,919
when Q or I don't know maybe when

370
00:20:52,200 --> 00:20:57,919
a scan code one for example when

371
00:20:59,539 --> 00:21:10,100
D when D is pressed then the scan

372
00:21:05,340 --> 00:21:10,100
code for it would be 0x20

373
00:21:10,380 --> 00:21:21,240
so I'll try to modify it to another code

374
00:21:17,179 --> 00:21:22,919
we just show that a key is pressed the

375
00:21:21,240 --> 00:21:25,919
simple if

376
00:21:22,919 --> 00:21:25,919
statement

377
00:21:25,980 --> 00:21:33,140
when al equals 0x20

378
00:21:30,659 --> 00:21:38,419
20

379
00:21:33,140 --> 00:21:42,140
then we will change the al to

380
00:21:38,419 --> 00:21:46,200
for example J we will change it to J

381
00:21:42,140 --> 00:21:50,520
and it's 24.

382
00:21:46,200 --> 00:21:52,620
so let's also there are also I I don't

383
00:21:50,520 --> 00:21:56,640
want to explain this particle in detail

384
00:21:52,620 --> 00:22:01,340
but there's only there is a

385
00:21:56,640 --> 00:22:05,159
press key and after that a released key

386
00:22:01,340 --> 00:22:07,760
is sent so we'll also try to change the

387
00:22:05,159 --> 00:22:07,760
release key

388
00:22:08,039 --> 00:22:12,140
so if D is

389
00:22:12,559 --> 00:22:21,919
release yeah

390
00:22:16,080 --> 00:22:21,919
for the first scan code then 0xa

391
00:22:21,960 --> 00:22:24,740
percent

392
00:22:25,980 --> 00:22:35,880
so if zero if the key code is equals

393
00:22:30,539 --> 00:22:38,179
to 0xa0 then we will change it

394
00:22:35,880 --> 00:22:38,179
to

395
00:22:40,220 --> 00:22:46,740
0xA4 because we want to change the D

396
00:22:45,179 --> 00:22:49,380
with j

397
00:22:46,740 --> 00:22:54,900
uh

398
00:22:49,380 --> 00:22:59,100
0xA4 yeah that should be fine

399
00:22:54,900 --> 00:23:03,919
Also let's print

400
00:22:59,100 --> 00:23:03,919
some keys here like

401
00:23:04,159 --> 00:23:10,520
e is

402
00:23:07,559 --> 00:23:10,520
pressed

403
00:23:13,380 --> 00:23:21,000
now let's run this script I try to

404
00:23:17,700 --> 00:23:24,140
just because we are using the same

405
00:23:21,000 --> 00:23:27,539
snapshot I think is better to rerun

406
00:23:24,140 --> 00:23:29,179
this snapshot but anyway let's just

407
00:23:27,539 --> 00:23:33,480
try to

408
00:23:29,179 --> 00:23:36,799
return to the controller here I try to

409
00:23:33,480 --> 00:23:36,799
rerun the snapshot

410
00:23:43,740 --> 00:23:49,940
again connect it because because the

411
00:23:46,380 --> 00:23:49,940
addresses are not changed here

412
00:24:27,299 --> 00:24:31,880
I'm just waiting because individually

413
00:24:30,179 --> 00:24:35,400
wants to connect

414
00:24:31,880 --> 00:24:39,120
and now it's connected

415
00:24:35,400 --> 00:24:41,460
try I will try again and now this thing

416
00:24:39,120 --> 00:24:44,960
transitions performed so let's just copy

417
00:24:41,460 --> 00:24:44,960
the same script

418
00:24:56,520 --> 00:25:02,360
as we can see here when a is pressed

419
00:24:59,520 --> 00:25:06,480
then we will see how the key press

420
00:25:02,360 --> 00:25:08,100
instructions here but if anyway I press

421
00:25:06,480 --> 00:25:10,980
d

422
00:25:08,100 --> 00:25:13,679
things change then it's changed to J

423
00:25:10,980 --> 00:25:17,039
like I'm currently pressing D and you

424
00:25:13,679 --> 00:25:22,740
can see that it's depressed but

425
00:25:17,039 --> 00:25:26,520
meanwhile G is shown here

426
00:25:22,740 --> 00:25:28,980
and J is shown here excuse me

427
00:25:26,520 --> 00:25:32,460
uh

428
00:25:28,980 --> 00:25:34,740
and if I press J

429
00:25:32,460 --> 00:25:39,320
then it's just like a regular

430
00:25:34,740 --> 00:25:39,320
instructions but when I press D here

431
00:25:39,779 --> 00:25:45,320
uh

432
00:25:41,159 --> 00:25:45,320
it changes to J

433
00:25:46,460 --> 00:25:54,960
so this was a simple example of how we

434
00:25:51,059 --> 00:25:58,260
can use hyper identity to use to debug

435
00:25:54,960 --> 00:26:00,539
different IO devices we could extend

436
00:25:58,260 --> 00:26:03,120
our knowledge and combine it with s

437
00:26:00,539 --> 00:26:05,820
script engine to perform more complex

438
00:26:03,120 --> 00:26:08,880
scenarios and this was just a simple

439
00:26:05,820 --> 00:26:11,640
case that shows how you can perform

440
00:26:08,880 --> 00:26:16,020
keylogging by a hypervisor even modifying

441
00:26:11,640 --> 00:26:19,380
the resources the physical resources in

442
00:26:16,020 --> 00:26:23,580
summary and in this section we see how

443
00:26:19,380 --> 00:26:25,860
external interrupts are used as a way to

444
00:26:23,580 --> 00:26:28,740
communicate to notify the processor

445
00:26:25,860 --> 00:26:31,320
about different events and we will see

446
00:26:28,740 --> 00:26:33,539
we also saw different ways of

447
00:26:31,320 --> 00:26:36,360
communication between peripherals like

448
00:26:33,539 --> 00:26:39,120
PMIO and MMIO and the differences

449
00:26:36,360 --> 00:26:43,620
between these two methods and finally in

450
00:26:39,120 --> 00:26:46,020
the hands-on section we monitor the IO

451
00:26:43,620 --> 00:26:49,020
ports for PS2 keyboards and wrote a

452
00:26:46,020 --> 00:26:51,779
simple the script to change some of the

453
00:26:49,020 --> 00:26:54,120
keys in the keyboard before finishing as

454
00:26:51,779 --> 00:26:57,539
I mentioned before there are some tools

455
00:26:54,120 --> 00:27:00,360
here that used to show the base address

456
00:26:57,539 --> 00:27:03,600
for MMIO devices the tools that are used

457
00:27:00,360 --> 00:27:08,400
in devices HW info also some of the

458
00:27:03,600 --> 00:27:11,940
descriptions derived from this source so

459
00:27:08,400 --> 00:27:15,080
nothing more thanks for watching and see

460
00:27:11,940 --> 00:27:15,080
you in the next section

